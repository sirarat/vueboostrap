module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? '/~cs62160311/learn_bootstrap/'
    : '/'
}
